package com.example.commandlineapp;

import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommandLineAppApplication implements CommandLineRunner {

  @Autowired
  private MyAppService appService;

  public static void main(String[] args) {
    SpringApplication.run(CommandLineAppApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    if (args.length == 1) {
      Path absolutePath = appService.run(args[0]);
      System.out.println(absolutePath.toString());
    }
  }

}
