package com.example.commandlineapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * service
 */
@Service
public class MyAppService {

  private static final String _OUTPUT_TXT = "output.txt"; // output text file
  private static final int _THRESHOLD_DETRACTOR = 4 * 60;// number of minutes after which a customer in not happy
                                                         // (become a detractor)
  private static final int _THRESHOLD_PROMOTTER = 2 * 60;// number of hour for an happy customer

  private static Logger LOG = LoggerFactory.getLogger(MyAppService.class);

  /**
   * 
   * @param fileName
   * @return the path to the result file
   * @throws Exception
   */
  public Path run(String fileName) throws Exception {

    List<Input> inputs = readInputFile(fileName);

    LOG.info("Let's analyse what the drone has to do.");

    List<List<Input>> permutations = getPermutations(inputs);

    List<Input> bestPermutation = getTheBestPermutation(permutations, now()); // the drone starts now! Let's hope it's
                                                                              // not too late.

    LOG.info("Found the best roadmap for the drone!!");

    return writeResultFile(bestPermutation, now());
  }

  /**
   * get now. Ex "07:30:12"
   * 
   * @return the present hour minutes secondes
   */
  private String now() {
    Calendar c = Calendar.getInstance();
    return c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
  }

  /**
   * write the result file on the file system
   * 
   * @param bestPermutation
   * @return Path object
   * @throws IOException
   */
  private Path writeResultFile(List<Input> bestPermutation, String droneStartTime) throws IOException {

    Path path = Paths.get(_OUTPUT_TXT);
    Path absolutePath = path.toAbsolutePath();

    // Use try-with-resource to get auto-closeable writer instance
    try (BufferedWriter writer = Files.newBufferedWriter(path)) {
      for (Input input : bestPermutation) {
        writer.write(input.getId() + " " + input.getDelivery() + "\n");
      }
      writer.write("NPS " + getNps(bestPermutation, droneStartTime) + "\n");
    }
    return absolutePath;
  }

  /**
   * get the permutation which has the best NPS score
   * 
   * @param permutations
   * @param droneStartTime
   *          the time when the drone starts to deliver
   * @return
   */
  public List<Input> getTheBestPermutation(List<List<Input>> permutations, String droneStartTime) {
    List<Input> bestPermutation = permutations.get(0); // initialization of the best permutation randomly (here it is
                                                       // the first permutation)
    int maxNps = getNps(bestPermutation, droneStartTime);
    for (List<Input> p : permutations) {
      int nps = getNps(p, droneStartTime);
      if (nps > maxNps) {
        maxNps = nps;
        bestPermutation = p;
      }
    }
    getNps(bestPermutation, droneStartTime); // to set the delivery time correctly
    return bestPermutation;
  }

  /**
   * get the NPS value for a given scenario
   * 
   * @param inputs
   * @return NPS value
   */
  private int getNps(List<Input> inputs, String droneStartTime) {
    int detractors = 0;
    int promoters = 0;
    long cumulativeWait = 0; // number of minutes since the drone started.
    for (Input input : inputs) {
      int distance = input.getDistanceToWarehouse();
      cumulativeWait += distance; // to drone goes to the client
      long minutesOfWait = getWaitingPeriode(input, cumulativeWait, droneStartTime);
      LocalDateTime delivery = getDateTime(input.getTimestamp()).plusMinutes(minutesOfWait);
      input.setDelivery(delivery.getHour() + ":" + delivery.getMinute() + ":" + delivery.getSecond());
      if (minutesOfWait > _THRESHOLD_DETRACTOR) {
        detractors++;
      } else if (minutesOfWait < _THRESHOLD_PROMOTTER) {
        promoters++;
      }
      cumulativeWait += distance; // to drone goes back to the warehouse
    }
    return 100 * (promoters - detractors) / inputs.size();
  }

  /**
   * 
   * @param input
   *          the client order
   * @param wait
   *          number of minutes that previous deliveries took
   * @param droneStartTime
   *          when the drone started operate
   * @return number of minutes the client has to wait for his delivery
   */
  private long getWaitingPeriode(Input input, long wait, String droneStartTime) {
    LocalDateTime fromDate = getDateTime(input.getTimestamp());
    LocalDateTime toDate = getDateTime(droneStartTime);
    return ChronoUnit.MINUTES.between(fromDate, toDate) + wait;
  }

  /**
   * get LocalDateTime
   * 
   * @param timestamp
   * @return
   * @throws NumberFormatException
   */
  private LocalDateTime getDateTime(String timestamp) throws NumberFormatException {
    String[] orderTime = timestamp.split(":");
    // the year and the day don't matter here because we assume it's the same day
    return LocalDateTime.of(2018, 12, 19, Integer.parseInt(orderTime[0]), Integer.parseInt(orderTime[1]),
        Integer.parseInt(orderTime[2]));
  }

  /**
   * get all permutations possible. It will give us all the scenarios possible
   * 
   * @param inputs
   * @return
   */
  public List<List<Input>> getPermutations(List<Input> inputs) {
    List<List<Input>> result = new ArrayList<>();

    // start with an empty list
    result.add(new ArrayList<Input>());

    for (int i = 0; i < inputs.size(); i++) {
      List<List<Input>> temp = new ArrayList<>();

      // go through the list from previous round and add the number into the list
      for (List<Input> each : result) {
        for (int index = 0; index <= each.size(); index++) {
          List<Input> dest = new ArrayList<>(each); // make a copy of original list
          dest.add(index, inputs.get(i)); // insert the number into all possible positions
          temp.add(dest);
        }
      }

      // switch over to prepare for the next round
      result = temp;
    }
    return result;
  }

  /**
   * Read input file
   * 
   * @param fileName
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   */
  public List<Input> readInputFile(String fileName) throws FileNotFoundException, IOException {
    InputStream in = new FileInputStream(new File(fileName));
    BufferedReader br = new BufferedReader(new InputStreamReader(in));
    String read;
    List<Input> input = new ArrayList<Input>();

    while ((read = br.readLine()) != null) {
      input.add(new Input(read));
    }

    br.close();
    in.close();
    return input;
  }

}
