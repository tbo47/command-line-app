package com.example.commandlineapp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommandLineAppApplicationTests {

  private static final String INPUT_TXT = "./input.txt";
  @Autowired
  private MyAppService appService;

  @Test
  public void contextLoads() throws Exception {

    List<Input> inputs = appService.readInputFile(INPUT_TXT);

    List<List<Input>> permutations = appService.getPermutations(inputs);

    List<Input> bestPermutation = appService.getTheBestPermutation(permutations, "07:00:00"); // the drone starts
                                                                                              // delivering at 7am

    bestPermutation.stream().forEach(System.out::println);

    assertThat(inputs.size(), is(4));
    assertThat(bestPermutation.get(0).getId(), is("WM002"));

  }

}
